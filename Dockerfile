FROM registry.gitlab.com/juhani/go-semrel-gitlab:v0.20.4 as release
FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest as gitlab-terraform
FROM registry.gitlab.com/gitlab-org/release-cli:latest as gitlab-release-cli

FROM alpine:3.12
COPY --from=release /usr/bin/release /usr/local/bin/release
COPY --from=gitlab-terraform /usr/bin/gitlab-terraform /usr/local/bin/gitlab-terraform
COPY --from=gitlab-release-cli /usr/local/bin/release-cli /usr/local/bin/release-cli
COPY --from=hashicorp/vault:1.7.3 /bin/vault /usr/local/bin/vault

RUN apk add --no-cache \
        bash \
        build-base \
        ca-certificates \
        cargo \
        curl \
        docker \
        docker-compose \
        gcc \
        git \
        gnupg \
        go \
        jq \
        libc-dev \
        libffi-dev \
        make \
        nodejs \
        npm \
        openssh-client \
        openssl \
        openssl-dev \
        py3-pip \
        python3 \
        python3-dev \
        rust \
        rsync \
        wget \
        yarn \
        zip \
    && update-ca-certificates \
    && pip3 install --upgrade pip


# install aws cli
RUN pip3 --no-cache-dir install --upgrade awscli

# install kubectl v1.23.6 as latest version has a problem using client.authentication.k8s.io/v1alpha1
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.6/bin/linux/amd64/kubectl && \
    install ./kubectl /usr/local/bin && \
    kubectl version --client

# install k9s
RUN curl -Lo k9s.tgz https://github.com/derailed/k9s/releases/download/v0.25.1/k9s_Linux_x86_64.tar.gz && \
	tar -xf k9s.tgz && \
	install k9s /usr/local/bin/

# install helm
RUN curl -SsL -o helm.tar.gz https://get.helm.sh/helm-v3.5.2-linux-amd64.tar.gz && \
    tar -xf helm.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/ && \
    helm version && \
    rm helm.tar.gz

# Install glibc (dependency for tfswitch)
RUN wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
  && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.32-r0/glibc-2.32-r0.apk \
  && apk add glibc-2.32-r0.apk
# install terraform using tfswitch and install `tf` shortcut to run tfswitch before terraform
RUN curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | bash \
    && tfswitch --latest
COPY bin/tf /usr/local/bin/tf

# install yq for parsing and converting terrafarm json output \
RUN YQ_VERSION=v4.27.3 \
    && YQ_BINARY=yq_linux_amd64 \
    && wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/${YQ_BINARY}.tar.gz -O - | tar xz && mv ${YQ_BINARY} /usr/bin/yq

# install kops
RUN KOPS_VERSION=v1.25.0 \
    && curl -Lo kops https://github.com/kubernetes/kops/releases/download/${KOPS_VERSION}/kops-linux-amd64 \
    && chmod +x kops \
    && mv kops /usr/local/bin/kops

ENV GOPATH /go
ENV GOROOT /usr/lib/go
ENV PATH "$GOPATH/bin:$PATH"
RUN apk add --no-cache musl-dev bc
RUN mkdir -p $GOPATH/src $GOPATH/bin
RUN go get -u golang.org/x/lint/golint

COPY entrypoint.sh /entrypoint.sh
CMD chmod +x /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
