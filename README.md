# Builder Image

## built-in commands

* docker
* aws cli
* terraform
* ansible
* [go-semrel-gitlab](https://juhani.gitlab.io/go-semrel-gitlab/)

## Entrypoint
You may specify the following environment variables

| environment variable | description                                                                                           | example                                            |
|:---------------------|:------------------------------------------------------------------------------------------------------|:---------------------------------------------------|
| DOCKER_LOGIN         | specify credentials for a docker registry. Specify multiple logins by separating them with a `,`.     | `foobert\|secretpass\|hub.docker.io,foo\|bar\|baz` |
| SSM_SSH_KEY          | fetch ssh key from ssm and add it to the ssh agent                                                    | `path/to/ssm/key`                                  |
| GL_TOKEN             | specify a personal gitlab access token for use with go-semrel-gitlab (tag/commit/push access to repo) | `218377baffc1a`                                    |
| DEBUG                | show shell command output. does a `set -x` at the entrypoint script                                   | `true`, `1`                                        |
| GPG_PRIVATE_KEY      | specify a gpg private key to import                                                                   | ``                                                 |

These environment variables are available:

| environment variable | description                                        | example         |
|:---------------------|:---------------------------------------------------|:----------------|
| `CONTAINER_ID`       | the id of the builder-image container              | `76b5b1fce7c5`  |
| `MOUNT_NAME`         | the id of the volume that holds the git repository | `d827c7ad797ff` |

## terraform

There are two versions available for terraform:

* v0.11.13 (default)
* v0.12.0

If v0.12.0 should be used, you have to set this environment variable `USE_TF_12=true`, otherwise leave it empty or unset

## Examples

Spawn container with mount:

```
# run a busybox with this project mounted at /builds/systems/builder-image
$ docker run -v ${MOUNT_NAME}:/builds -w /builds/${CI_PROJECT_PATH} busybox ls -la
```

## TODO

* add fs stat checks for `/usr/bin/python` ansible has a hard dependency on that
