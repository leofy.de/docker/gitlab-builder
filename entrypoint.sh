#!/bin/bash

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

# registry logins
if [ -n "${DOCKER_LOGIN}" ]; then
    IFS=',' read -r -a DOCKER_LOGIN_ARR <<< "${DOCKER_LOGIN}"
    for DOCKER_LOGIN_ITEM in "${DOCKER_LOGIN_ARR[@]}"; do
        if [[ -n "${DEBUG}" ]]; then
            echo LOGIN ITEM: "${DOCKER_LOGIN_ITEM}"
        fi
        IFS='|' read -r -a LOGIN_ARR <<< "${DOCKER_LOGIN_ITEM}"
        if [ ${#LOGIN_ARR[@]} -eq "3" ]; then
            USER="${LOGIN_ARR[0]}"
            PASS="${LOGIN_ARR[1]}"
            REGISTRY="${LOGIN_ARR[2]}"
            docker login --username "${USER}" --password "${PASS}" "${REGISTRY}"
        else
            echo "could not parse login: ${DOCKER_LOGIN_ITEM}"
            echo "expected \"{USER}|{PASS}|{REGISTRY}\""
        fi
    done
fi

if [ -n "${GPG_PRIVATE_KEY}" ]; then
    gpg -v --import <(echo "$GPG_PRIVATE_KEY")
fi

exec "$@"
